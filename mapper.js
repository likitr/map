/**
 * Created by vvvla on 14.04.2017.
 */
    /*
    * Требования к HTML:
    * контейнер для карты с ID "#map-inner"
    * кнопка поиска по адресу c ID "#butt_addr"
    * поле ввода адреса с ID "#user_address"
    * поле ID "#place_id"
    * не забыть подключить google maps api
    * */
var markers = [];
var map;
/**
 * @description Initializing map. Инициализация карты.
 */
function initMap() {
    var Niko = {lat: 46.965, lng: 31.997};
        map = new google.maps.Map(document.getElementById('map-inner'), {
        zoom: 15,
        center: Niko
    });
    geocoder = new google.maps.Geocoder();
}
/**
 * @description Отслеживание кнопки в обертке document.ready .
 */
$(function () {
    $("#butt_addr").click(function () {
        deleteMarkers();
        geocodeAddress($('#user_address').val().trim() + ' Николаев');

       /* geocodePlaceID($('#place_id').val().trim());*/
    })
});

/**
 * @description Searching location by ID on map and calling for adding marker. Alert if error while geocoding.
 * @description Поиск по Айди и добавление маркера.
 *
 * @param {string} ID_string Your ID. Ваш Айди.
 *
 */
function geocodePlaceID(ID_string) {
    geocoder.geocode({'placeId': ID_string}, function (result, status) {
        if (status === 'OK') {
            $('#place_id').val(result[0].place_id);
            addMarker(result[0].geometry.location, geocoder);
        }
        else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    })
}
/**
 *
 * @description Searching location on map and calling for adding marker. Alert if error while geocoding.
 *
 * @description Поиск по адресу и добавление маркера.
 * @param {string} address String from #user_address. Строка из инпута #user_address.
 *
 */
function geocodeAddress(address) {
    geocoder.geocode({'address': address}, function (result, status) {
        if (status === 'OK') {
            $('#place_id').val(result[0].place_id);
            addMarker(result[0].geometry.location, geocoder);
        }
        else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    })
}
/**
 * @description Initialising marker and tracks dragend of it. Pushes marker to markers array.
 * @description Инициализация маркера. Добавляет маркер в массив маркеров.
 * @param {object} location Latlng из результатов поиска.
 *
 * */
function addMarker(location) {
    map.setCenter(location);
    map.setZoom(17);
    var marker = new google.maps.Marker({
        position: location,
        draggable: true,
        map: map
    });
    /**
     * @description Tracking end of dragging of just created marker.
     * @description Отслеживание конца перетаскивания маркера.
     * */
    marker.addListener('dragend', function(event) {
        map.setCenter(event.latLng);
        geocoder.geocode({'location': event.latLng},function (result, status) {
            if (status === 'OK') {
                refreshInputs(result);
            }
        })
    });
    markers.push(marker);
}
/**
 * @description Refresh #place_id input & #user_address input according to results of geocoder.
 * @description Обновление инпутов соответственно с результатами поиска.
 * @param {object} result Result of geocoding. Результат геокодирования.
 *
 * */
function refreshInputs(result) {
    var route,street_number;
    $('#place_id').val(result[0].place_id);
    route = findTypes(result,'route');
    street_number = findTypes(result,'street_number');
    $('#user_address').val(route+ ' ' +street_number);
}
/**
 * @description Check if results of geocoding contains variable with needed property.
 * @description Проверка содержит ли результат геокодирования переменную со свойством needed_type.
 * @param {object} result Result of geocoding. Результат геокодирования.
 * @param {string} needed_type Type needs to find in results of geocoding. Тип, который требуется найти.
 *
 * @return {string} name Искомое значение.
 *
 * */
function findTypes(result,needed_type) {
    for ( var i = 0; i < 3 ; i++ ) {
        var type = result[0].address_components[i].types[0],
            name = result[0].address_components[i].short_name;
        if (type === needed_type) {
            return name;
        }
    }
}
/**
 * @description Deleting all markers from map and array "markers".
 * @description Удаление всех маркеров.
 * */
function deleteMarkers() {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }
    markers = [];
}